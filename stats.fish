#!/usr/bin/env fish

function activity
    http --timeout 1 "https://"$argv[1]"/api/v1/instance/activity" \
        | jq -r '.[] | [.[]] | @csv' \
        | sed s/\"//g \
        | sed s/^/$argv[1],/g \
        | grep -vi "not implemented" | grep -vi "not found" | grep -vi "bad login" >> stats.csv
end


set peers (http https://mastodon.social/api/v1/instance/peers | jq -r .[])
set npeers (count $peers)
echo $npeers" peers to mastodon.social"

echo "server,week,statuses,logins,registrations" > stats.csv
activity mastodon.social
set n 0
for peer in $peers
    set n (math 1 + $n)
    echo $n" / "$npeers" ("$peer")"
    activity $peer
end
